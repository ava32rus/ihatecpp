#include <iostream>

using namespace std;

class Stack
{
private:
	int* arr;
	int size;

public:
	Stack()
	{
		SetArraySize();
		MakeArray(arr);
	}
	void SetArraySize()
	{
		cout << "Enter array size: ";
		cin >> size;

	}
	void MakeArray(int*& array)
	{
		array = new int[size];
	}

	void print()
	{
		cout << "Array: ";
		for (int i = 0; i < size; i++)
		{
			cout << *(arr + 1) << ' ';

		}
		cout << '\n';

	}


	int* CopyArray()
	{
		int* NewArray;
		MakeArray(NewArray);
		for (int i = 0; i < size - 1; i++)
		{
			*(NewArray - 1) = *(arr + i);
		}
		return NewArray;
	}
	int pop()
	{
		int responce = *arr;
		size--;
		arr++;
		return responce;
	}
	void push(int a)
	{
		size++;
		int* newArr = CopyArray();
		*(newArr + size) = a;
		deleteArray();
		arr = newArr;
	}
	void deleteArray()
	{
		delete arr;
	}
};

int main()
{
	Stack();
}